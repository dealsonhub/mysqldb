-- MySQL dump 10.13  Distrib 5.7.20, for Linux (i686)
--
-- Host: localhost    Database: dealsonhub
-- ------------------------------------------------------
-- Server version	5.7.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `dealsonhub`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dealsonhub` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `dealsonhub`;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` bigint(20) NOT NULL,
  `isApproved` bit(1) NOT NULL,
  `categoryName` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (11,'','Products','e003c431-3a6d-4ac8-af6d-bc4b2af68844'),(13,'','Tourism','111b4e12-3982-4fdd-9e74-8a82668ccb34'),(15,'','Financial','a1d8623f-c13c-46bc-ba31-d3d0407df8b1'),(17,'','Automobile','cd9b8daa-5c8b-4201-9eb7-bc27ecd19955'),(19,'','Entertainment ','99a5c521-ff86-4664-88d7-6defb0103708'),(238,'','Markets','a873d634-771b-4069-b48b-6623dbdf85cf'),(245,'','Education','a2f377f0-53da-4610-98ce-242ebc49de3b');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company`
--

DROP TABLE IF EXISTS `company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company` (
  `id` bigint(20) NOT NULL,
  `isApproved` bit(1) NOT NULL,
  `comapnyWebSite` varchar(255) DEFAULT NULL,
  `companyName` varchar(255) DEFAULT NULL,
  `companyPhoto` varchar(255) DEFAULT NULL,
  `facebookPage` varchar(255) DEFAULT NULL,
  `storeName` varchar(255) DEFAULT NULL,
  `category_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKht3vlqtsq2u55a52n96twbk68` (`category_id`),
  CONSTRAINT `FKht3vlqtsq2u55a52n96twbk68` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company`
--

LOCK TABLES `company` WRITE;
/*!40000 ALTER TABLE `company` DISABLE KEYS */;
INSERT INTO `company` VALUES (367,'\0','','The Boulevard Arjaan','dc1f5baf-42eb-4af7-a159-51beaf50b81c','','The Boulevard Arjaan',19),(373,'\0','',' Media Plus','8ff9f5d7-651f-41e0-9ebb-31614ebc13ad','',' Media Plus',19),(378,'\0','','Hackmanite','b03dd9a4-b548-42d5-a21f-9a72879361fa','','Hackmanite',19),(383,'\0','','Carrefour Haypermarket','deba63fe-139f-494f-bb34-8b1054cafa7e','','Carrefour Haypermarket',238),(388,'\0','','Carrefour Supermarket','d5195d58-d751-4f08-b496-dfaccea69cc7','','Carrefour Supermarket',238),(393,'\0','','Sameh Mall','e2543160-ed31-4859-ad40-52c75d40f95e','','Sameh Mall',238),(398,'\0','','C - Town','8bcf0235-5af2-4986-99fc-e189e7b5ae1b','','C - Town',238),(403,'\0','','Al Farid Stores','8ef2c3d2-57f7-4954-927d-32bbdee3c520','','Al Farid Stores',238),(408,'\0','','City Mall','4fb7f763-6570-4547-a815-b73921bf5a79','','City Mall',238),(414,'\0','','TAJ Lifestyle Mall','62e124c3-056a-460d-83e3-0e271d77e1a4','','TAJ Lifestyle Mall',238),(420,'\0','','Al Abdali Mall','0bae396c-f657-4ce2-8595-6c01ccb1152b','','Al Abdali Mall',238),(426,'\0','','Royal Jordanian','7128ce0f-7151-48ef-9ff7-f5d7105bb842','','Royal Jordanian',13),(431,'\0','http://www.rattan-house.com','RATTAN HOUSE','e077fd4b-9eda-4216-9ade-ea54f54c7008','','RATTAN HOUSE',11),(437,'\0','','Beit Shams','49f0bb5d-cbb4-4e72-9cb5-ac21c32c3a82','','Beit Shams',19),(443,'\0','','A Little Italy Christmas','0ca0058e-e681-4ee3-a704-2f9912b6a8dc','','A Little Italy Christmas',19),(448,'\0','','IKEA','6bdf2e43-f950-4289-8b4f-6e1d5b79a7b9','','IKEA',11),(454,'\0','','Essentials Spa','8ae175c7-6595-4795-a14d-cb43257613d5','','Essentials Spa',11),(484,'','','Sixt rent a car ','18a55eeb-a4ab-4239-a4f4-e23864a873ec','https://www.facebook.com/pg/sixt/events/?ref=page_internal','Sixt rent a car ',17);
/*!40000 ALTER TABLE `company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `companyContactInfo`
--

DROP TABLE IF EXISTS `companyContactInfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `companyContactInfo` (
  `id` bigint(20) NOT NULL,
  `isApproved` bit(1) NOT NULL,
  `contactPerson` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `mobileNumber` varchar(255) DEFAULT NULL,
  `phoneNumber` varchar(255) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `location_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKntds3mppa0yhtsjks5meor495` (`company_id`),
  KEY `FK2pdwon07xvpqwgis0yqsc1qwx` (`location_id`),
  CONSTRAINT `FK2pdwon07xvpqwgis0yqsc1qwx` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`),
  CONSTRAINT `FKntds3mppa0yhtsjks5meor495` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `companyContactInfo`
--

LOCK TABLES `companyContactInfo` WRITE;
/*!40000 ALTER TABLE `companyContactInfo` DISABLE KEYS */;
INSERT INTO `companyContactInfo` VALUES (368,'\0',NULL,'','','','',367,365),(374,'\0',NULL,'','','','',373,365),(379,'\0',NULL,'','','','',378,365),(384,'\0',NULL,'','','','',383,365),(389,'\0',NULL,'','','','',388,365),(394,'\0',NULL,'','','','',393,365),(399,'',NULL,'','','','',398,365),(404,'\0',NULL,'','','','',403,365),(410,'\0',NULL,'','','','',408,409),(416,'\0',NULL,'','','','',414,415),(422,'\0',NULL,'','','','',420,421),(427,'\0',NULL,'','','','',426,365),(433,'\0',NULL,'suhair@rattan-house.com','','','065814639',431,432),(439,'\0',NULL,'','','','',437,438),(444,'\0',NULL,'','','','',443,415),(450,'\0',NULL,'','','','',448,449),(456,'\0',NULL,'','','','',454,455),(485,'',NULL,'','','','',484,365);
/*!40000 ALTER TABLE `companyContactInfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company_subCategories`
--

DROP TABLE IF EXISTS `company_subCategories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company_subCategories` (
  `Company_id` bigint(20) NOT NULL,
  `subCategory_id` bigint(20) NOT NULL,
  PRIMARY KEY (`Company_id`,`subCategory_id`),
  KEY `FK1cdw0ktidsc0vt4cacef3bdla` (`subCategory_id`),
  CONSTRAINT `FK1cdw0ktidsc0vt4cacef3bdla` FOREIGN KEY (`subCategory_id`) REFERENCES `subCategories` (`id`),
  CONSTRAINT `FKkj2j0ddm7rcvcyawtufadpe6s` FOREIGN KEY (`Company_id`) REFERENCES `company` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company_subCategories`
--

LOCK TABLES `company_subCategories` WRITE;
/*!40000 ALTER TABLE `company_subCategories` DISABLE KEYS */;
INSERT INTO `company_subCategories` VALUES (454,26),(431,30),(448,30),(426,33),(484,38),(408,318),(414,318),(420,318),(383,319),(388,319),(393,319),(398,319),(403,319),(367,339),(373,339),(378,339),(437,339),(443,339);
/*!40000 ALTER TABLE `company_subCategories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contactUs`
--

DROP TABLE IF EXISTS `contactUs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contactUs` (
  `id` bigint(20) NOT NULL,
  `companyName` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `firstName` varchar(255) DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  `messageBox` varchar(255) DEFAULT NULL,
  `mobileNumber` varchar(255) DEFAULT NULL,
  `phoneNumber` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contactUs`
--

LOCK TABLES `contactUs` WRITE;
/*!40000 ALTER TABLE `contactUs` DISABLE KEYS */;
INSERT INTO `contactUs` VALUES (463,'XXXXXX','eiad.3maish@gmail.com','EIAD','As3ad','fgfgg','0799797172','0799797172');
/*!40000 ALTER TABLE `contactUs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dealsOnHubInfo`
--

DROP TABLE IF EXISTS `dealsOnHubInfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dealsOnHubInfo` (
  `id` bigint(20) NOT NULL,
  `aboutus` longtext,
  `terms` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dealsOnHubInfo`
--

LOCK TABLES `dealsOnHubInfo` WRITE;
/*!40000 ALTER TABLE `dealsOnHubInfo` DISABLE KEYS */;
/*!40000 ALTER TABLE `dealsOnHubInfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fivePhoto`
--

DROP TABLE IF EXISTS `fivePhoto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fivePhoto` (
  `id` bigint(20) NOT NULL,
  `fifthImage` varchar(255) DEFAULT NULL,
  `firstImage` varchar(255) DEFAULT NULL,
  `fourthImage` varchar(255) DEFAULT NULL,
  `secoundImage` varchar(255) DEFAULT NULL,
  `thirdImage` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fivePhoto`
--

LOCK TABLES `fivePhoto` WRITE;
/*!40000 ALTER TABLE `fivePhoto` DISABLE KEYS */;
INSERT INTO `fivePhoto` VALUES (152,'5be87511-073f-46aa-9c81-e9d608751632','2553a652-a376-4ac2-8180-e39ae2703c7f','a7740403-f553-4113-b3d5-ed32316656cb','bf2720eb-eff7-4e63-8586-5678cd310df8','837927e4-004d-4306-a52c-805e0c68eaba');
/*!40000 ALTER TABLE `fivePhoto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` VALUES (547),(547),(547),(547),(547),(547),(547),(547),(547),(547),(547),(547),(547),(547),(547),(547);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `imgMapping`
--

DROP TABLE IF EXISTS `imgMapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `imgMapping` (
  `id` bigint(20) NOT NULL,
  `imagePath` varchar(255) DEFAULT NULL,
  `imageUrl` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `imgMapping`
--

LOCK TABLES `imgMapping` WRITE;
/*!40000 ALTER TABLE `imgMapping` DISABLE KEYS */;
INSERT INTO `imgMapping` VALUES (1,'/var/lib/tomcat7/webapps/ROOT/WEB-INF/classes/logo.jpeg','logo'),(6,'/images/e9a6c99c-54bc-4224-abc2-4d8bdc3665c4food-beans-coffee-drink.jpg','a366386e-9aeb-461a-8ca3-9eb94a8f04ac'),(7,'/images/c135d057-ce0d-45cb-bac4-14f7c3aabe55tmp.jpg','9aa9cc28-e4bc-4b5d-aa2d-e149de768ee9'),(10,'/images/aa8e21d5-5ded-48a1-bfe1-d75b75112053download 8.jpg','2ce657a1-516a-44cc-b481-a00b67dd417c'),(12,'/images/e5d1d5aa-6714-42da-82ba-f3c6aa934fecimages 5.jpg','fe12a0e2-05aa-48f7-afa2-9d7ad3a249ab'),(14,'/images/90a9a16e-f46b-4213-aa41-ef1100045dc6download 2.jpg','c2bf3d04-671f-48d1-86f4-da81d93d1197'),(16,'/images/37ed44db-6190-4bd1-ab1a-beb1e15f9d22images 7.jpg','4935e2cc-9f85-4b43-bbc2-8175eebd67f4'),(18,'/images/22899db1-93b6-421f-9ad9-d4a141045289download 4.jpg','05167ee7-72ea-4783-a87b-af7a5e49b0a8'),(20,'/images/6ebcf522-1d66-4480-bf06-f562832474a4500_F_31121836_3MZZPdEHKMoCNGOrz2HW9HXzi7re1fWv.jpg','e003c431-3a6d-4ac8-af6d-bc4b2af68844'),(21,'/images/7382d0f6-3e51-4439-a251-cb4fcc93f7a5Tourism.jpg','0690ea1c-f381-41ce-bdae-7adb43c9b199'),(22,'/images/72917c94-1bc8-4e0f-9179-6c967b541407Financial servicies.jpg','0c2459cb-f545-43b7-a375-723830eb2d11'),(23,'/images/9bbe6d9d-64be-4e76-a655-54b28e3d1585Automobile.jpg','80ff1cb3-5a76-4319-8233-8b044cf7374f'),(24,'/images/846c53a2-6ef3-4c28-afce-8278bdec9655Entertainment.jpg','3f5d77ea-b763-4d1a-98cc-77f538da5ea2'),(43,'/images/397a40e2-1f51-4968-bf17-84387ea22d04123.jpg','9a2c1cfa-342d-4c80-be20-5aa561ed09d8'),(44,'/images/ef7f0e26-32b9-42f8-a47b-47a3e7b3db7a123.jpg','e604c5f2-f588-4747-ab76-e141624ff131'),(47,'/images/18786a3c-5c3e-4bf4-9edb-735aa015c0bf123.jpg','b2dc9c7d-41f7-46f3-b25b-1bab73c18a4f'),(49,'/images/2580e134-f97d-4695-a1f7-48486b85c1c1adl.jpg','13bd7d35-7156-47f4-b89c-8fc5424803d7'),(52,'/images/4fb63f9d-f378-4a03-b6bb-20ef5cdac31fadl.jpg','f6fe839e-832a-4d0c-9143-41e4415216e9'),(54,'/images/b6923a17-ed6c-4fcc-9e24-6b4600968ee4Aishti.jpg','01636881-427e-4ab2-b4da-912d6567359d'),(57,'/images/49120338-b497-4c08-8d83-59e7185a0ccfAishti.jpg','42f80419-eeb3-4a32-b463-3b80a8eb073f'),(59,'/images/0457b82f-aa84-48a5-a957-10bf756b4fc3american-eagle.jpg','65f8b93e-6812-4359-b328-791857146b6c'),(62,'/images/3ed2713d-4a71-407f-9be0-90becd2d267aamerican-eagle.jpg','68097391-de75-4402-93a7-87a73799caa8'),(64,'/images/b6b44ea7-8325-4d9d-9468-03558e76cda8Call_It_Spring.jpg','b53192b3-712d-4d1c-9296-60031efef69f'),(67,'/images/30bd67fd-2ef5-43f1-ae2b-f41a4fbb6994Call_It_Spring.jpg','1c8a45a5-90ee-45f0-9ced-1b1fd05c09d5'),(69,'/images/156c2e29-4642-4995-a0d2-7733039901b5calliope.jpg','f71f5061-10bd-4249-84ea-d6e3eb15b871'),(73,'/images/65d82f77-876e-4f22-961b-c2f815fb4e9ccalliope.jpg','ff8d4dd7-abbd-490f-afd8-67f75c050896'),(75,'/images/8ba54ad6-7909-4829-8cbd-a5e2764bcd1fcoast.jpg','dd250644-84e0-4046-a462-3dc8e145713a'),(79,'/images/db235167-f964-47b4-a3b4-592e71d95d35charles.jpg','7755c611-6e9f-4fcc-b0e8-ba9a3e2118a3'),(82,'/images/fca8f42b-c738-43f7-b4d8-6211ce90f5e1charles.jpg','42c72033-2e51-4788-8cbf-c950957275cb'),(84,'/images/8e6abc06-fb3e-49b9-ad87-57789747f702diesel.jpg','3d5124ef-38f3-4425-af99-669bb94e27b1'),(87,'/images/f39763a9-2ab3-4b73-a452-d0fddfbc5052diesel.jpg','fe0e6fdd-52c1-4578-85ef-0552b7ecced6'),(89,'/images/ff969b9a-faca-414b-8293-d4c9db46f31fGS.jpg','dc8773fa-9f54-42c4-a059-2af89c3ac8e8'),(95,'/images/94c0f8f4-fa1a-4e45-b3a0-68d83f71c41fGS.jpg','0de27344-e8fc-4f5f-8591-208973863e33'),(97,'/images/32f620b3-2c48-4cc0-bbd1-5e11596f06d4Garage.jpg','0312a1b5-2f3b-4baf-8c05-5e0f0f947b3d'),(100,'/images/6c27a11e-8e3f-44fb-b1b4-530852640843Garage.jpg','539c7c99-556f-4bda-80ca-65735be955bb'),(102,'/images/8c32f09a-5e3d-408c-8e90-81fed64667f2guess.jpg','ac50cd17-7332-4f4a-ad9e-0d6d75ff4da0'),(105,'/images/5e8420a2-d179-4aa5-91b1-d35c0f85a219guess.jpg','fa52b727-ba7c-4cca-9955-3ac54a86ac77'),(107,'/images/b5cde47b-f5c3-42c0-b90a-c02227a49d93lasenza.jpg','a28204fe-8289-4307-a908-8255a7b0de8b'),(110,'/images/41a3d1c4-ea60-4ef6-bd86-08f477713176lasenza.jpg','8eb976ac-eca9-460d-a6a0-b249c1302c8f'),(112,'/images/818ead47-6d72-44fc-99f6-9b86444dc712gravity.jpg','096a33f3-a875-4220-b14e-1bc99824f925'),(115,'/images/5ce33148-066d-473e-b20e-c53217f70d00gravity.jpg','95294852-0be6-4a2b-a745-b6bbe0cc31b7'),(117,'/images/f9f19895-4508-41f7-891f-afcfcf2a155dKaren-Millen.jpg','012b985e-826b-43c6-9a5d-124da7b1c99f'),(120,'/images/d19a27e7-58b1-41a2-9f32-a3ca166f77fb2989-christmas-bow-powerpoint-template.jpg','f72e00d6-4c83-41ee-882b-49c89a172155'),(122,'/images/44165e35-c8a7-45cd-a4f6-99bd6d110eb4zara.jpg','0d55ed60-63e9-496e-b99c-aa1ae4fd9225'),(125,'/images/e17001db-5276-4d46-a7e1-0f9a93782db6cyber-monday-deals-design-creative-concept-vector-background-for-web-and-mobile-applications-illustration-template-design-business-infographic-page-banner--148565010.jpg','df663574-feba-49bc-80ec-31c2209a757e'),(127,'/images/3b6f1ab1-ab5d-4fe3-9f85-bd484c96d84fabdSamad.jpg','1cccac02-9ddd-4c18-a6b7-51a5ca6610a2'),(130,'/images/d48b4678-2bae-480f-b12f-7a5a178401a2abdSamad.jpg','abd81e31-8002-4e94-87a9-e79cf47a9670'),(132,'/images/563a3c6d-dbc3-4650-8035-c79bf2b88dd7Bath & Body Works.jpg','8723d0f0-1463-41c0-b48f-e47f77b8c5b1'),(136,'/images/2444826d-5b89-4594-9b9f-c55e58186a6dBottega-Logo.jpg','6da2019a-b45e-4987-956f-ccb9628da822'),(139,'/images/e298b5db-05b9-4ec3-b3e7-93167913af03cyber-monday-deals-design-creative-concept-vector-background-for-web-and-mobile-applications-illustration-template-design-business-infographic-page-banner--148565010.jpg','e1d83bb0-bf45-4113-878f-cca7af3be3ed'),(141,'/images/def9fa11-c76a-4218-8f0e-f1dc53e0ffa5Incoco.jpg','6be378c3-94c1-4aa2-ba68-3b9a38bbaa89'),(144,'/images/5fbd9a8d-1809-4e27-b642-a986bcbf4062Automobile.jpg','64afd608-6a40-4847-81f9-b84655976abf'),(147,'/images/b371b254-35f8-4d80-ad21-6ec17c46efb4download 2.jpg','32229c5f-71cb-4bf4-a66f-83f3b32a480f'),(148,'/images/64b791dd-957f-4b32-8b3b-5c98d0abe1cfdownload 3.png','8115d8e9-7570-40fb-aa62-ced1ad950fb1'),(149,'/images/a394657e-2536-4bf4-8cef-02a83dbc8528download 4.jpg','6ee83749-7da3-41da-9c25-47dc23593902'),(150,'/images/7d978cb9-3a97-4104-a66a-c287d5953266images 5.jpg','28a3d132-f079-4a1d-b5b8-29022443ebd0'),(151,'/images/6a3d4b28-c930-46fa-a58d-b83601abc177images 7.jpg','782381e5-dc37-42cd-840d-d8ceebd53e0d'),(153,'/images/ac701d80-7760-415c-b0f0-4279701c29dbAirline 1.jpg','2145a0a8-4284-43d1-aae1-34b1369dcb4a'),(156,'/images/468f06a7-813e-4e83-ab73-831a088e7dcdAirline 1.jpg','70596ad5-3c6a-4112-aefb-07e65e1dc8ab'),(157,'/images/3f9d05d5-606c-4c5f-bccf-800899b3fab4Airline 2.jpg','0f4e377d-e7f3-4751-9e90-9625b494b005'),(159,'/images/a87ca33e-72a2-4838-834a-28794639ed57Airline 3.jpg','232f75ea-77e6-44fa-acbd-af927fbb1d7d'),(162,'/images/085165cd-a3d8-4896-bcc9-1131d0472a10Airline 3.jpg','668d6b70-54c8-4acf-a5c1-3e1e813f93e2'),(164,'/images/d4f30dbf-d06b-458c-9323-7ce971067e18Hotel 1.png','c53f6d54-0f93-4e7a-8e31-8047be362a33'),(168,'/images/b35e996e-7883-4d48-aa30-4c5995ae9b21Hotel 2.png','359976d7-03f6-4967-8d63-33e131e90b96'),(170,'/images/1af52ad4-11cb-4516-a8f6-e1fcf94b9ff3Travel 1.jpg','b3564ec1-2490-4694-b68c-26df54d84012'),(173,'/images/fecb41be-9fd9-484e-a181-5633972e6401travel 2.jpg','16785887-9b00-4dc8-8129-7f832ab086f0'),(175,'/images/821a6472-f6a0-4272-b493-73d509715100Bank 1.png','ad002b76-397e-4dd2-a88d-f6fb2e44fd37'),(178,'/images/b80c82fe-52cc-48c7-a577-1e14cf21cb2fBank 2.jpg','6b549a61-3253-4326-8a1b-d814396965ed'),(180,'/images/eb3172d2-bb92-4178-a362-0ddd4b084797Agent 1.png','cf801e0c-bf5d-492c-a099-c0125deb3d29'),(183,'/images/87e301e4-a412-4672-abdb-7dc8022e3e7bAgent 2.jpg','924e26f4-e73b-4c73-a65e-8ff50d878e2b'),(185,'/images/1a7345d7-d1d2-4e94-b560-22c05861139ediverto.jpg','654f4a55-1477-4bbc-9461-cb6022c85f87'),(188,'/images/1730d44e-0b1a-4c73-97cd-08841dccaf72ingraveit.jpg','ad6d4799-e061-4572-86e4-7f8a51277c65'),(190,'/images/fa4848e5-e8d8-43cc-b04d-e39efcc88304Jayland.jpg','8ae8870e-aa4c-4832-b1f7-884efad1bac5'),(194,'/images/466e951e-b22e-4821-af56-c86781269e2bdownload (1).jpg','025b58a7-898b-4206-941e-15f33d98e2a4'),(195,'/images/f3e47a3e-fc9a-4370-9eb8-b35bf742d840download.jpg','5172d06a-d2d1-4f60-9282-858324e507c5'),(196,'/images/541d847f-1727-46a0-8fa6-cc488ff82aa9images.jpg','ba2ab5aa-4d63-4064-8ecb-7771fbbd67c0'),(206,'/images/fbe00630-ac13-4124-b7aa-0c081ba26ff7Bath & Body Works.jpg','35179326-67ee-4ac6-b13b-76c4fc0d876b'),(207,'/images/6a41c144-c8a1-48e9-a280-3de9212156eeC5.jpg','51d018b2-7009-4495-82a3-e104eea20feb'),(208,'/images/38d71c2f-2a37-48e0-b2b9-4ec57e2fa25aC4.jpg','7b213598-5597-4c9e-9f52-0e64cdda6fea'),(209,'/images/2e45357e-2967-4765-b158-cb6052d46e9aC3.jpg','d11d5f3a-cce0-4202-b1a4-97936a148dbd'),(210,'/images/96653711-0d4f-48fd-ad09-bc626d4e30dfC2.jpg','cf2a9efa-d14b-4cf0-a27e-5133c8591b57'),(211,'/images/09fa9819-d183-4279-b86c-674cf873c284C1.jpg','bb8e8633-5e5a-423f-9332-7a7593aa2a90'),(212,'/images/501f5cd1-85d9-4465-a84d-e01ce655a588C6.jpg','9014200f-c250-4fc5-a384-d994463b2b76'),(213,'/images/317adc5e-439b-4349-ae82-5c0557ac7358C7.jpg','36280181-7da8-4bac-902f-931b9a16e832'),(214,'/images/61290ea3-892c-4d66-87d0-a87d1dd7dab1Tourism.png','37a6b798-2f50-4308-9889-ed39741d67e6'),(215,'/images/e71e1810-5afe-452a-a0cf-0274843b79f9Automobile.png','ee2971a8-5866-4f6d-a4db-576d87b1cb37'),(216,'/images/f76b9787-76b3-4ec2-bc3e-d0b97c56d8b1Entertaiment.png','74f94eb2-bd8f-4df7-b07e-38322fa37a31'),(217,'/images/b6807a23-0c8d-417c-8d24-9d378230511bFligjhts.png','8a0f457d-11d8-4632-a8ef-b52778fd18fe'),(218,'/images/f0f8da3f-da3d-40e2-9d6c-093be8fef84fHotels.png','e800adaf-6f8b-4291-81ce-5d2c19849662'),(219,'/images/94b3da61-ca8e-4d9b-8126-9916c37e97c4Travel agencies.png','984fe4d5-4086-4b5e-b8e1-75ac1de6625f'),(220,'/images/90608665-c0ed-40c7-9574-e085b014665dFinancial lservices.png','1ff30968-1795-4aaf-9c0d-b28ed8b86486'),(221,'/images/e8fe6eff-ad35-4246-9320-10f314f94798Banks.png','4ef6eb20-2f4f-44e8-aa74-aafcce7f8bc4'),(222,'/images/3f07f3f9-320c-4ad9-8567-a06f0672d114Money Exchange.png','86d61094-5448-40c7-b314-5defd1d8fdc1'),(223,'/images/656150d3-9cc3-4727-964c-be010957685aWatches.png','0289a2d4-a997-4a74-bf82-91d5838c03d7'),(224,'/images/6d64b016-a586-41d8-8831-697f9c21e05bFashion.png','2f938310-4f6f-4c01-bb7b-90c2ead3c04d'),(225,'/images/19d223df-5d3e-4e71-a447-fb5ff9b1183fHealth and Beauty.png','5857132a-a229-4ecb-88d7-557f2b155b63'),(226,'/images/6695e4ec-ce08-4107-a84f-b14cb9f71450Furniture.png','c71e86ae-3f70-4f17-b8d7-ffa60627fa02'),(227,'/images/11c10cb6-dbba-4ffc-8c15-1ae3e7cd1440Optics.png','44857a31-7ec5-4a0f-9bf1-ac8195babe94'),(228,'/images/4f6d0cfe-855e-4b5f-b014-23db027e4818hanger.png','443d6a69-d6ff-410e-8ecc-4514ef100f67'),(229,'/images/b4b85d66-0e02-4cdb-98b9-e1eb23cb76f8hanger.png','04182532-119d-4150-ab7a-090719d830d6'),(230,'/images/08b33e70-dd07-45d3-b0c3-95813902692fcoat (1).png','9730bf31-fbfd-4657-a9aa-4701141e4bb2'),(231,'/images/ae16f1da-45d0-4179-b23e-5205ff51dda0Automobile.png','fc068c35-8239-4e0d-ae9e-0f523672db3e'),(232,'/images/c761ff5b-15ce-4a42-847b-f218aca6cc36123.jpeg','2997731e-cd04-4a18-80ab-1a5d3c6357a4'),(233,'/images/01a79034-5188-4ae2-b2db-4eafe552b020123.jpeg','42cfd252-9f35-40e2-a716-68172b42a5b1'),(234,'/images/58d9f1b5-b75f-486c-a5b5-9e804f6378cc2-.jpg','9ea167bd-d158-44c7-ac46-a36caf401256'),(237,'/images/6830e7b9-1e76-4369-b6fe-2cd752aa2623hypermarket.jpg','006d4933-744c-49dc-8387-ffb68e4112f6'),(239,'/images/b0aefb69-b7a1-4c32-a4d2-f16ea19779452782976_150304102017_Logo.jpg','40360a9f-3520-4d78-b2e2-f531bd506c36'),(240,'/images/21b31109-b316-4604-aa2b-0ed565bf219dHypermarkets.png','067cbedf-1779-4cb7-b9fe-d5f78e2c0bc7'),(242,'/images/c87cfd58-a22c-4485-a31e-937ba8700b771.jpg','64ed30da-7be0-40a6-96f2-bb0aa915f47d'),(243,'/images/6ad35961-3962-45da-80e5-01bda0c3d24f2.jpg','3451c7cd-f236-47c3-be11-58277d671d53'),(246,'/images/0aca4fb2-75d7-481d-974d-eeecae53d0ddCollege.jpg','38bd1764-fb3e-4d00-9710-2d685a3ec688'),(248,'/images/e5ea422d-12e6-41c0-b9f1-8d9115dccc7cTraining.jpg','a29c9559-55fd-48ca-b4c6-3feb9a3d77a0'),(250,'/images/71e91776-5904-4296-9aa8-0d7413e2806aCarrefour.png','9032099f-8b8c-48c8-b3a5-bdfb389b6444'),(252,'/images/f94c3a87-4e9a-4990-95fa-2b98e703a243C-TOWN.jpg','4b209387-4725-4ad2-b5cd-5c307489bab6'),(254,'/images/00cfa78b-3153-48a1-889f-eb10c0100b5dSameh Mall.png','3879323a-66f6-4c1a-aa6a-b9f8b9436ed2'),(265,'/images/e676e58e-8987-45d8-8432-bea39343e008Carregour offer 1.jpg','801fb7bf-9102-4c9c-9497-a8181128c88c'),(267,'/images/78b1339b-8936-4078-9b5b-4ededfb7a8e4Carregour offer 2.jpg','db16a071-2ff0-497c-848d-eeb2d274ff17'),(269,'/images/9b127f88-91c6-4c7c-b22e-31b0f0740c9esameh offer 1.jpg','b2a2f9f3-6cef-4ae1-b9c2-9a75ec573fc8'),(271,'/images/1e1a1dc0-074d-4d03-8ca8-d6f65f83fbdbsameh offer 2.jpg','b0c7a256-5444-4825-82f2-673ae8a992d1'),(273,'/images/facf4c9a-2d0a-4cdd-bba1-31a12aebce28sameh offer 3.jpg','faa338ae-87f2-45ef-9680-1720fe2edd4a'),(275,'/images/c9ee81a0-43ac-460e-a25c-a6b3059d7da4al quds university.jpg','2aa66ee1-df10-4a95-8d25-04e4301e154f'),(277,'/images/dcf4bbef-908c-4984-b2c0-23c31b803ebbKIC.png','59e1fe82-1bd5-418b-a1e4-8bc3683857bc'),(281,'/images/f4af131f-64a0-4182-8c7b-032d60d6eaebal quds offer.jpg','f4e0aacf-c868-432f-a422-e91a888cc261'),(283,'/images/bc060a86-a172-4e55-8687-3f27e1b83a93KIC TRAINING OFFER.png','fba3a4d0-f0a6-4472-9600-bc141975813b'),(285,'/images/bf496de8-910e-4068-a7c5-c60de6e8dc9aKIC.png','67195172-91b4-4058-87ef-0d6576a3295d'),(287,'/images/45b92dbc-2582-4b8d-81a8-ec53edca371dComming Soon.png','c61da579-36e6-4c70-98ce-38090ecf9118'),(288,'/images/3f33f6ab-e300-474e-ba90-35b5a3708ec01.jpg','fb2af01f-86d9-4d81-8b97-eff54140e3a7'),(289,'/images/dc8748ea-656f-4af8-9605-c9da55c9a91e2.jpg','c7167a86-5c6d-4995-bf82-931957a0810f'),(290,'/images/0e172e97-1ece-4478-98d2-d103a54ce4f63.jpg','5c1c3d9f-9c70-4c91-a00f-4a4e7c4efdb9'),(291,'/images/499a299c-e096-4bc9-a1e6-f1c268311ab04.jpg','cbebe161-cbcc-45c0-a31f-c55d6bcbafee'),(292,'/images/1c193f77-e6e7-4bdc-8ad6-eb36e3a0bf455.jpg','538ef9d1-5ed4-4b8b-9818-48d3e5cc8c87'),(293,'/images/90bb01e7-59b9-4d77-af31-dd5361a0ae28apollo-xmas-sale.jpg','44b8dec4-2023-4b40-8b68-1d61462a30a9'),(294,'/images/7b5d7362-5dde-4aff-8990-8bfc38fae13a2017.jpg','7e3de072-242f-41d0-b999-91411afa2b63'),(295,'/images/ab49180d-6876-488a-8bfd-8e39f7d21ce92018.jpg','cf21e6bf-ec20-480a-a63a-99a780db868f'),(296,'/images/baff6386-c8a4-4095-a483-6c7c91bcbc772017.jpg','0addfb1e-f7c4-4c6a-b98d-1c965989930f'),(297,'/images/3088bf43-5192-49cc-a6ef-6cc7ce2c6c52croppedImg_501761164.jpeg','7a414708-876f-4bf4-801e-10951831c731'),(298,'/images/beccc122-82af-420d-a585-b11a0a10cf0aFather-Christmas.jpg','d4f4e0c4-b0ec-4249-9d40-f39d8db9b870'),(299,'/images/be488abf-4049-4bc0-bd89-abcb5a7c0e3a1.jpg','e3d236eb-6db0-459d-b13a-041b6682dc01'),(300,'/images/70590499-1e8d-41be-a7a0-cc9387ec0e1f3.jpg','84318c73-60cc-4137-bdcb-fe7908ca8ee8'),(301,'/images/579209e4-acee-4ee1-9112-4994226f00064.jpg','c5c601b2-480e-4dcd-a8dc-55138aece3da'),(302,'/images/387e850b-b645-49e3-b4a5-12c1416594f7Tourism.png','ef748a0f-22c9-4ccd-a1dc-f09bc3f6f1e8'),(303,'/images/a0237c09-021a-4b7d-957b-6218fd587bd3Finance.png','a1d8623f-c13c-46bc-ba31-d3d0407df8b1'),(304,'/images/464a1c32-e1af-4065-8456-6058ce650f2eAutomobile.png','19a188de-3872-4cb3-b597-e5efc5bb9824'),(305,'/images/6740e8a8-1414-4d9b-9421-802f9e180de2Education.png','a2f377f0-53da-4610-98ce-242ebc49de3b'),(306,'/images/b8ce508e-e38e-4b7e-8075-1e4097effcc6Entertaiment.png','99a5c521-ff86-4664-88d7-6defb0103708'),(307,'/images/79a00356-7ec3-465d-880d-55761b84fc40Markets.png','a873d634-771b-4069-b48b-6623dbdf85cf'),(308,'/images/edc9e4a7-a221-4416-9aad-d1ce4f620ae8Automobile.png','cd9b8daa-5c8b-4201-9eb7-bc27ecd19955'),(309,'/images/453a69f1-5f94-46dc-a0b6-42720dd9c5a9hotel.png','f216c678-e84c-44d7-bd34-952c11f86105'),(310,'/images/8d169776-1b62-4df9-b037-eb63633efe05Flight.png','974fe2e5-bf81-4bd1-86db-0282ef874807'),(311,'/images/a9aff520-9b10-43a0-91b9-df020268af52travel office.png','d0189063-0b27-48c4-a4d8-18ba49ed3ee0'),(312,'/images/fc137650-f348-407d-89fc-d660745dd756Bank.png','886b9fad-3530-43d1-92d0-cbb4c3f1a718'),(313,'/images/590c6560-fc72-4098-9646-eba32034110cExchange office.png','afe5e8e4-2bd1-4506-a708-de4988f06f30'),(314,'/images/4b47d5a4-90dc-4d40-9e8d-87686b83356aCar agency.png','d7ff2f5f-799d-4203-bf32-f45d8b508a79'),(315,'/images/090fcd1d-dcac-4211-a3be-294c7e9d1878car rental.png','8f469994-2e0c-4712-82a1-39978c42da5a'),(316,'/images/a23acf90-0dc5-4ff6-8b0b-6c08c4090a4eUniversity.png','b3729620-0b6a-4cb5-a8ed-49c18e60345d'),(317,'/images/f19b9ae2-be63-4103-ba4e-2379088d650cTraining.png','e5c3324a-85a2-4894-876a-dbbd6f42b4a8'),(320,'/images/5bd20f39-ebab-47d7-831f-b953b11c2772Mall.png','cc7a83b3-96c1-4ded-95ec-3158714bd5d7'),(321,'/images/9cd5e4fb-8e68-4c05-b4af-b4f4ef9f2026Hypermarkets.png','432b786a-c0b0-4641-a6e8-9c51e32abfa2'),(322,'/images/2119c45c-f487-4434-af60-085d9df6e56cFurniture.png','0072fabc-44a0-4038-b2a2-b3cdac4db766'),(323,'/images/daa3a678-47ac-4297-9ec0-255767ca072aWatches.png','f0d6eaef-a0e7-429e-a0f3-a255c9a90bd3'),(324,'/images/ff97225e-916b-4ec2-bc09-7045ecb2b00bOptics.png','7ac128ad-4322-4e4c-b44e-4aa5edc671e3'),(325,'/images/f1e8349e-42aa-44be-8fad-cfb39edf5f2bFashion.png','b1aa4f67-a269-491d-a84f-04b9161c24aa'),(326,'/images/aacba5f4-192e-4d30-bba3-4da2ac16eb3cBeauty.png','50cfe8de-71e4-47aa-8964-476aa74448e2'),(327,'/images/c19527c6-a280-40a1-afc8-ca844d6c8a19Shoes.png','8a2d6aff-6328-412f-8226-26e7bad127ff'),(329,'/images/b75a8b4b-c972-4416-88ab-6c3900e8d6a8Electronics.png','80216f70-6de5-49eb-972f-f2e75dbc4a4c'),(330,'/images/f60c7d9a-7b8a-4124-899a-00319777c7cfCinema.png','3857c76a-b6e2-4657-86c5-380bad259d10'),(331,'/images/1d4bc823-96d3-4c73-9771-99ba2d7919a2Health club.png','340dc90d-133a-43c4-929a-40565edbf644'),(332,'/images/184d5ea3-9948-4661-96b5-59ce44d10e7eDance.png','78e731ba-488a-4003-a217-ea9f973dd28b'),(333,'/images/750c341a-009d-40b6-a42c-02d75b3915a7Kids Play area.png','7990eb67-85cd-47ee-858e-fa7357635528'),(334,'/images/b282f3d4-adf0-4103-8840-f94493be340cToys.png','4f36c059-76ae-4977-877c-0d0eb654d9f1'),(335,'/images/23c2dade-7b14-4c21-88c1-f8581e8e4494bowling.png','da98d7dc-2f86-4c36-b965-34df5ac0378f'),(336,'/images/fb858ade-a070-426b-80e3-1ee9e9942f39Sport.png','29eb8afc-3497-4ccc-a577-356dea00048e'),(338,'/images/f5635d3a-bc61-440f-867d-159a3292f1d8Theater.png','5bf53e8c-8550-4a35-8b46-725300015d95'),(340,'/images/1d750114-c6a6-46f4-b66b-b73bbe872f821.jpg','2553a652-a376-4ac2-8180-e39ae2703c7f'),(341,'/images/b36a815b-9804-4155-9b37-dd67ab187b832.jpg','bf2720eb-eff7-4e63-8586-5678cd310df8'),(342,'/images/62af3ef4-bdce-424c-9856-ab95f5c97a3c3.jpg','369ac831-ad9e-4d9a-b66d-2fd2ae152daf'),(343,'/images/af3be612-6ec1-47f3-a5bc-ae94dfcf6b3d4.jpg','a7740403-f553-4113-b3d5-ed32316656cb'),(344,'/images/e83e7741-06c3-4930-b83d-1a1471480c605.jpg','5be87511-073f-46aa-9c81-e9d608751632'),(345,'/images/fd24c8c0-87e9-45b7-b58d-524e49a65b636.png','837927e4-004d-4306-a52c-805e0c68eaba'),(346,'/images/5c42289f-c3c3-4150-b665-6238d0f30e76123.jpg','111b4e12-3982-4fdd-9e74-8a82668ccb34'),(357,'/var/lib/tomcat7/webapps/ROOT/WEB-INF/classes/icon.png','icon'),(358,'/var/lib/tomcat7/webapps/ROOT/WEB-INF/classes/google-play-badge.png','google'),(359,'/images/968f993b-bb24-4003-ab5c-5017d2e2b66dimages.jpg','15474479-f473-40fe-b339-f52575e2d7a2'),(361,'/images/11c156da-a0aa-431c-b086-fad652deb64bRotana Hotel company logo.png','f79bf1ee-0cac-4474-9b74-ccd3fca704f5'),(363,'/images/e235fe53-d1bd-49ea-a3d9-ebccbfeb3491swiss christmas market amman offer logo.jpg','d50e292e-0598-4feb-af58-9cad779d6ade'),(366,'/images/58a482f7-37d2-4e13-a191-58bdec1edbc8The Boulevard Arjaan offwe 1.jpg','dc1f5baf-42eb-4af7-a159-51beaf50b81c'),(369,'/images/7d7633e3-f544-46cf-a5f6-9ae2a4d2cfadThe Boulevard Arjaan offwe 1.jpg','60443b06-34aa-4da9-9abf-3228582bec49'),(372,'/images/de1714a1-48a1-4625-8ad6-4a88af6401d9christmas parade offer logo.jpg','8ff9f5d7-651f-41e0-9ebb-31614ebc13ad'),(375,'/images/60ec7f3c-d859-48e8-905f-929077328d1echristmas parade offer logo.jpg','9fbdec9c-6dc4-4c04-957c-9e15609b8fb1'),(377,'/images/c1bb1f9a-1e24-4d44-aae3-95850f90a2c112 nights to christmas  offer logo.jpg','b03dd9a4-b548-42d5-a21f-9a72879361fa'),(380,'/images/caeb1ba9-a563-46fd-bb10-7c94571bd2dd12 nights to christmas  offer logo.jpg','7feda713-eedf-41bd-bad6-144a2e64cbb5'),(382,'/images/e1bb184e-87b8-4876-9475-d918256e2207Carrefour logo.jpg','deba63fe-139f-494f-bb34-8b1054cafa7e'),(385,'/images/8e816250-c0bf-49f1-a704-199df33ee709Carrefour Hypermarket offer logo 1.jpg','0971770e-6f50-40a5-83a6-bdaa50cded5d'),(387,'/images/2902f349-fa4b-4398-945d-46b83302809bCarrefour logo.jpg','d5195d58-d751-4f08-b496-dfaccea69cc7'),(390,'/images/cbeee88e-c876-4086-b84d-b6a8e239144fCarrefour Supermarket offer logo 2.jpg','e19295f3-e13e-44dd-9fd8-9bb4a92891fc'),(392,'/images/3ccea1da-fa43-46dc-bb0e-15c881091e0aSameh Mall logo.png','e2543160-ed31-4859-ad40-52c75d40f95e'),(395,'/images/c8eaf44a-7495-4048-9c04-5aa7479eb1c3Sameh Mall offer logo 1.jpg','87ec95db-d2aa-4df7-9831-1cf3e4987946'),(397,'/images/ba9fe1cd-35d9-46cd-844a-e2dc66843839C Town company logo.png','8bcf0235-5af2-4986-99fc-e189e7b5ae1b'),(400,'/images/bd0cddab-bd3c-4ec3-bae0-8c04a1d174d6C Town offer logo.jpg','a06c9d9c-fb63-432c-a88d-689109566f57'),(402,'/images/27211d14-73e3-4587-bf49-b4d4324f7f94Al Farid Stores Companylogol.jpg','8ef2c3d2-57f7-4954-927d-32bbdee3c520'),(405,'/images/7e161313-b1dc-4ca4-906b-986057a434ddAl Farid Stoes offer logo 1.jpg','cf21223b-e49f-4274-99a5-8a2dd59e2ae0'),(407,'/images/e029d876-6482-4b4f-8f5c-60308cffe1c0City Mall logo.jpg','4fb7f763-6570-4547-a815-b73921bf5a79'),(411,'/images/db159847-1f94-4bb7-a929-493f9320c549City Mall offer.jpg','4fa306d8-38ef-4a87-a4af-3bde5524d250'),(413,'/images/229f1d07-224f-4aa5-95c6-3fafe6033f16Taj Mall logo.jpg','62e124c3-056a-460d-83e3-0e271d77e1a4'),(417,'/images/23e49ab4-dd53-4628-83b9-e9ece22a7755Taj Mall offer.jpg','f389de10-8816-4594-99c9-baa328591453'),(419,'/images/65056147-3615-413b-981d-c9854976b137Al Abdali Mall logo.jpg','0bae396c-f657-4ce2-8595-6c01ccb1152b'),(423,'/images/60827f73-7b92-4061-8701-5e3b37ae067bAl Abdali Mall offer.jpg','e4ed4712-eeb7-449c-bff0-ca2beac7c908'),(425,'/images/818bf8a5-2a8d-4671-b37f-75ebe2c175ebimages.jpg','7128ce0f-7151-48ef-9ff7-f5d7105bb842'),(428,'/images/3e005aa8-35ef-4dd3-909e-28eaf57dc50fRJ offer logo.jpg','74bcbc2f-94ac-443f-880d-119645acb215'),(430,'/images/dd28171e-c797-42dc-afce-ecc739dd8035Ratan House company logo.png','e077fd4b-9eda-4216-9ade-ea54f54c7008'),(434,'/images/7efa2a9d-a927-41e2-9509-de7abad0eccbRATTAN HOUSE offer logo.jpg','7e101ebb-41f3-40d5-9f11-bb3e062fc4a4'),(436,'/images/e53f97bc-1c2f-469e-860c-a741c74b783bChristmas Open House.jpg','49f0bb5d-cbb4-4e72-9cb5-ac21c32c3a82'),(440,'/images/130b815d-5e1a-4cea-878e-49f3a8a720b6Christmas Open House.jpg','d4c5a1e2-a04e-41ab-86de-8823fe1dc768'),(442,'/images/a66188c2-1768-4a1e-b053-7573075bf2f1A-Little-Italy-Christmas.jpg','0ca0058e-e681-4ee3-a704-2f9912b6a8dc'),(445,'/images/ea5efc2a-8c6b-4665-a53a-75ba665f1c59A-Little-Italy-Christmas.jpg','9d9b51d0-c4dc-4497-a38c-674367dc3b10'),(447,'/images/3e676ec3-749e-496e-8247-5b98b5ef360bIKEA Company logo.png','6bdf2e43-f950-4289-8b4f-6e1d5b79a7b9'),(451,'/images/6c4bcd27-090f-4610-890d-4e1b02faadb4IKEA offer logo.jpg','671f3249-d081-4492-9746-a9a47ee5ac6f'),(453,'/images/77fe052e-9387-401b-a3ec-09ecb5d4d97bEssentials Spa company logo.png','8ae175c7-6595-4795-a14d-cb43257613d5'),(457,'/images/956c9f5f-5bba-4ca8-885d-9c8e481d78a9Essentials Spa offer logo.jpg','1c45e58e-a356-4fb6-a321-df00fb308742'),(459,'/images/86982ed6-f8bf-4c12-98ac-52ae6a8cacc6Al Farid offer 3.jpg','29e77c92-2b43-44aa-ad4a-2b59d5cdaf01'),(461,'/images/be127627-3f27-4006-b049-462e27948c9bC-Town offer logo 2.png','1e05af61-a434-4212-b829-7f262599dfc8'),(464,'/images/e14182c6-fc13-4283-9ac8-a51cc963a4a7Carrefour Hypermarket offer logo 3.jpg','93d23ac8-5e8c-448f-843b-832b10fe0219'),(465,'/images/a9cd81b5-f4e2-4100-92b9-88e674cfaf41Carrefour Supermarket offer logo 4.jpg','2faf5dd2-5ca8-4e16-a5bb-010a15cc16f3'),(466,'/images/ac3fb62b-fb5e-489a-b7cd-01e39435d460C-Town offer logo 3.png','fb406b91-1f19-4528-b107-b08f4433023a'),(467,'/images/64f95af7-b558-4426-9189-13ab1029f208Al Farid offer 4.jpg','fc013d49-fb0c-4a66-a939-aefb1e6c9ffb'),(468,'/images/5c9d84b3-0efd-47f8-b35b-fc7751fc9449Sameh mall offer logo  2.jpg','b53bd522-aba9-4729-8de8-90b32ef3942d'),(469,'/images/f452cd4f-1c56-47ca-9b23-adbe877bef6eAl Farid offer 5.jpg','43cbff3e-f5c4-4909-a756-689954aa07c8'),(470,'/images/9a0e3807-e620-4235-b50d-517f0d93228bC-Town offer logo 4.png','cef65d79-3c36-45b6-894c-82ee86fb6318'),(471,'/images/986f688f-afd4-4850-be8f-d8e16911d00210-dithering-opt.jpg','24d07dc5-1d82-4885-ba63-973311c1e0b0'),(473,'/images/8715f38d-0807-4d9f-bf7e-7f60bf8de860Carrefour Hypermarket offer logo 6.jpg','c9168ce2-88a4-4a71-8532-2dcc0194da2e'),(475,'/images/e12d9e8a-3da0-46a7-9089-2bedfc4cafbcCarrefour supermarket offer logo  5.jpg','43736273-bc99-43fa-a6b5-9c516b141ac8'),(477,'/images/af9fb1d8-324a-4cfa-ba6e-3d86d40a9202Sameh mall offer logo  3.jpg','d5b84c7e-0f94-46b4-b8c0-4aa45516308f'),(478,'/images/3af23985-207e-46e3-8595-2ad88c38cbb3Al Farid offer 6.jpg','95ecf8ac-974d-4d13-996c-f0a6c0e0233a'),(479,'/images/dfc537ac-5174-442c-9c5a-4454605f1527IKEA offer logo 2.jpg','f4a3fb59-2760-46d1-be65-6835f032b953'),(481,'/images/e12af7ce-0ff8-4560-992e-faba17a5bea0IKEA offer logo 3.jpg','d0c493b7-2ee7-4252-9b59-b4b5a4f7cfc6'),(483,'/images/06192d39-05d9-4971-8124-520866d06b80Sixt rent a car company logo.png','18a55eeb-a4ab-4239-a4f4-e23864a873ec'),(486,'/images/9460694f-b3ff-4ac1-8caf-b1430f32fa8bSixt rent offer logo.png','c582146f-cc54-4825-8a31-39bb089c9d42'),(490,'/images/5a973f51-cb01-4b85-bb11-dd97dbace512Sixt rent offer logo.png','ebfefbad-45dc-497c-8b84-e58941172c0e'),(511,'/images/80fd9868-d937-4fa0-8c9c-28b2cd1fefdbSixt rent offer logo.png','a3a33745-a224-48e4-b677-488cb9297cdb'),(532,'/images/3866e884-470d-4fd5-aa21-3a8036e9c9c7Carrefour Supermarket offer logo 6.jpg','d4bf0362-b56b-4355-af20-db3031ea8113'),(533,'/images/a0111df4-4b79-445c-a1c5-ac19dc7b7b15Al Farid Stores offer logo 2.jpg','87bb88d7-24c1-45a5-bd2c-b07cc94e1e96'),(534,'/images/e5c857de-a279-4b4d-bc67-d2f54dfa028aAl Farid offer 7.jpg','3b81a5a7-c30e-4e4e-89ef-ef2a1b7d8083'),(535,'/images/beb1d2b1-2837-4dfe-b86e-a3eb4667e339C-Town offer logo 5.png','941ec83c-3957-42fd-bda0-09515e119217'),(536,'/images/c1b910d4-e8a2-4524-9529-062e730993cbCarrefour Supermarket offer logo 7.jpg','026a5d7e-9bdc-470f-92cb-0807d8756b11'),(537,'/images/00f0e7d5-07c9-445e-9684-275f71700d50Carrefour Hypermarket offer logo 7.jpg','a9f0c968-8a5e-4c8a-b76f-15c79531f59e'),(538,'/images/18d224ae-1f38-4632-bb40-f16e8a211b34Al Farid Stoes offer logo 9.jpg','ea50a3d7-055a-49fa-ad55-85e7fd96b661'),(539,'/images/1a7410be-49f8-42ac-9cf0-c4acfb4b9dedC-Town offer logo 6.jpg','36f5fb99-3fc4-427b-b580-5fc372c887f5'),(540,'/images/1f180c25-1cbd-488f-abf2-ec056e8aef49C-Town offer logo 7.jpg','9c93cb1d-727d-4c0a-b31b-42c9cfe9e699'),(541,'/images/58acea5a-2b16-40bf-9840-7b01b760ee87Al Farid offer 10.jpg','7a326b61-ad54-4ada-9017-18600174a4fe'),(542,'/images/6601a1e0-88b4-4bb3-a718-5602f3ed1c09Al Farid offer 13.jpg','b8d5f6b5-d466-4fe8-b372-8e35bfb73b81'),(543,'/images/281737b0-c7a0-4489-b428-863c6d5c95acAl Farid offer 14.jpg','3c391e3e-c1dd-4eb2-80b5-da29a74abc13'),(545,'/images/468de0ac-4176-401f-8d1d-93a840567a16Carrefour Hypermarket offer logo 8.jpg','e5edbad0-b318-466a-9f6c-574e7cbbdd5b'),(546,'/images/04f77d81-fc25-4d75-9450-4a746a459a1bCarrefour Supermarket offer logo 8.jpg','c7205d9d-5956-4724-b481-0710df5841a9');
/*!40000 ALTER TABLE `imgMapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `locations`
--

DROP TABLE IF EXISTS `locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `locations` (
  `id` bigint(20) NOT NULL,
  `isApproved` bit(1) NOT NULL,
  `area` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `mall` varchar(255) DEFAULT NULL,
  `street` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `locations`
--

LOCK TABLES `locations` WRITE;
/*!40000 ALTER TABLE `locations` DISABLE KEYS */;
INSERT INTO `locations` VALUES (365,'','','Amman','',''),(409,'\0','','Amman','City Mall',''),(415,'\0','','Amman','TAJ Lifestyle Mall',''),(421,'','','Amman','Al Abdali Mall',''),(432,'\0','','Amman','','Abdullah Ghosheh street'),(438,'\0','Jabal Amman','Amman','',''),(449,'\0','','Amman','','Queen Alia Airport'),(455,'\0','Abdoun Circle','Amman','','Prince Hashim Bin Al Hussein Street');
/*!40000 ALTER TABLE `locations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mainPageCompanys`
--

DROP TABLE IF EXISTS `mainPageCompanys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mainPageCompanys` (
  `id` bigint(20) NOT NULL,
  `eightCompany_id` bigint(20) DEFAULT NULL,
  `eighteenCompany_id` bigint(20) DEFAULT NULL,
  `elevenCompany_id` bigint(20) DEFAULT NULL,
  `fifheCompany_id` bigint(20) DEFAULT NULL,
  `fifteenCompany_id` bigint(20) DEFAULT NULL,
  `fourCompany_id` bigint(20) DEFAULT NULL,
  `fourteenCompany_id` bigint(20) DEFAULT NULL,
  `nineCompany_id` bigint(20) DEFAULT NULL,
  `ninteenCompany_id` bigint(20) DEFAULT NULL,
  `oneCompany_id` bigint(20) DEFAULT NULL,
  `sevenCompany_id` bigint(20) DEFAULT NULL,
  `seventeenCompany_id` bigint(20) DEFAULT NULL,
  `sexCompany_id` bigint(20) DEFAULT NULL,
  `sexteenCompany_id` bigint(20) DEFAULT NULL,
  `tenCompany_id` bigint(20) DEFAULT NULL,
  `thirteenCompany_id` bigint(20) DEFAULT NULL,
  `threeCompany_id` bigint(20) DEFAULT NULL,
  `tweentyCompany_id` bigint(20) DEFAULT NULL,
  `twelveCompany_id` bigint(20) DEFAULT NULL,
  `twoCompany_id` bigint(20) DEFAULT NULL,
  `company20_id` bigint(20) DEFAULT NULL,
  `company21_id` bigint(20) DEFAULT NULL,
  `company22_id` bigint(20) DEFAULT NULL,
  `company23_id` bigint(20) DEFAULT NULL,
  `company24_id` bigint(20) DEFAULT NULL,
  `company25_id` bigint(20) DEFAULT NULL,
  `company26_id` bigint(20) DEFAULT NULL,
  `company27_id` bigint(20) DEFAULT NULL,
  `company28_id` bigint(20) DEFAULT NULL,
  `company29_id` bigint(20) DEFAULT NULL,
  `company30_id` bigint(20) DEFAULT NULL,
  `company31_id` bigint(20) DEFAULT NULL,
  `company32_id` bigint(20) DEFAULT NULL,
  `company33_id` bigint(20) DEFAULT NULL,
  `company34_id` bigint(20) DEFAULT NULL,
  `company35_id` bigint(20) DEFAULT NULL,
  `company36_id` bigint(20) DEFAULT NULL,
  `company37_id` bigint(20) DEFAULT NULL,
  `company38_id` bigint(20) DEFAULT NULL,
  `company39_id` bigint(20) DEFAULT NULL,
  `company40_id` bigint(20) DEFAULT NULL,
  `company41_id` bigint(20) DEFAULT NULL,
  `company42_id` bigint(20) DEFAULT NULL,
  `company43_id` bigint(20) DEFAULT NULL,
  `company44_id` bigint(20) DEFAULT NULL,
  `company45_id` bigint(20) DEFAULT NULL,
  `company46_id` bigint(20) DEFAULT NULL,
  `company47_id` bigint(20) DEFAULT NULL,
  `company48_id` bigint(20) DEFAULT NULL,
  `company49_id` bigint(20) DEFAULT NULL,
  `company50_id` bigint(20) DEFAULT NULL,
  `company51_id` bigint(20) DEFAULT NULL,
  `company52_id` bigint(20) DEFAULT NULL,
  `company53_id` bigint(20) DEFAULT NULL,
  `company54_id` bigint(20) DEFAULT NULL,
  `company55_id` bigint(20) DEFAULT NULL,
  `company56_id` bigint(20) DEFAULT NULL,
  `company57_id` bigint(20) DEFAULT NULL,
  `company58_id` bigint(20) DEFAULT NULL,
  `company59_id` bigint(20) DEFAULT NULL,
  `company60_id` bigint(20) DEFAULT NULL,
  `company61_id` bigint(20) DEFAULT NULL,
  `company62_id` bigint(20) DEFAULT NULL,
  `company63_id` bigint(20) DEFAULT NULL,
  `company64_id` bigint(20) DEFAULT NULL,
  `company65_id` bigint(20) DEFAULT NULL,
  `company66_id` bigint(20) DEFAULT NULL,
  `company67_id` bigint(20) DEFAULT NULL,
  `company68_id` bigint(20) DEFAULT NULL,
  `company69_id` bigint(20) DEFAULT NULL,
  `company70_id` bigint(20) DEFAULT NULL,
  `company71_id` bigint(20) DEFAULT NULL,
  `company72_id` bigint(20) DEFAULT NULL,
  `company73_id` bigint(20) DEFAULT NULL,
  `company74_id` bigint(20) DEFAULT NULL,
  `company75_id` bigint(20) DEFAULT NULL,
  `company76_id` bigint(20) DEFAULT NULL,
  `company78_id` bigint(20) DEFAULT NULL,
  `company79_id` bigint(20) DEFAULT NULL,
  `company80_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKek0qjtqpcpu1hlu8eal5vyicr` (`eightCompany_id`),
  KEY `FKo3gre64yx2rd8vu2id7ufaeix` (`eighteenCompany_id`),
  KEY `FKq1c7uopujhpyanuu1gfntyasw` (`elevenCompany_id`),
  KEY `FKsg0lkgsydsnhrajt0t2e381kw` (`fifheCompany_id`),
  KEY `FKd06d0dbcictgjtivhpynmpg44` (`fifteenCompany_id`),
  KEY `FKfdeij7de7thhxk36ikx90dcn2` (`fourCompany_id`),
  KEY `FKekto6b0d3g62ls7lu8gjq5iuo` (`fourteenCompany_id`),
  KEY `FK9tsysdo6nxkf1dd1fupskx7kq` (`nineCompany_id`),
  KEY `FKjqr1k05s0ifrdybl78yik6pup` (`ninteenCompany_id`),
  KEY `FK1pbupffx26skdvluxy4cxmpiy` (`oneCompany_id`),
  KEY `FK9jlg9dtiryhm27a0ihli7pfmx` (`sevenCompany_id`),
  KEY `FKjwm573y4vbhmx49pmjq0xfxnd` (`seventeenCompany_id`),
  KEY `FKh81y1fqjq50vutjwdsa896w75` (`sexCompany_id`),
  KEY `FKe27nytws4aq4p12kngjupjpk1` (`sexteenCompany_id`),
  KEY `FKirbusr3y51hd5xqh3hfoh3wip` (`tenCompany_id`),
  KEY `FKiyk9nn4qtv5ev29s9x4i6vyl2` (`thirteenCompany_id`),
  KEY `FKs65u1e99qgl11wv1jk34k677m` (`threeCompany_id`),
  KEY `FKq0jth5fml3vgjqgguxj30hy5s` (`tweentyCompany_id`),
  KEY `FK7ghid732wnwhf6tftc9sqnlyx` (`twelveCompany_id`),
  KEY `FKmwy35hw8rg1b48iqmumum5pka` (`twoCompany_id`),
  KEY `FKpof3nfpaphir4t1k9sx418auy` (`company20_id`),
  KEY `FK3fppbvryta115yvs0nk8ckei8` (`company21_id`),
  KEY `FKhu1ia8uk4cjypfssv49nsco1l` (`company22_id`),
  KEY `FK9yw7tv6k1jvjmmkvrvttuu9o2` (`company23_id`),
  KEY `FK7hf101w4xe7bxrp4vtkt1a50n` (`company24_id`),
  KEY `FK16ha652i3audy2ijs3r18kla6` (`company25_id`),
  KEY `FKlsl6srh5ytr26qrl3utydw3nj` (`company26_id`),
  KEY `FKovlm0ew67yjq00vv2t1i3iiy5` (`company27_id`),
  KEY `FKqoj55o5evcr2ysi4c5yk4qeq` (`company28_id`),
  KEY `FK2ve9vwaog26al4ptifnvwpnfl` (`company29_id`),
  KEY `FK22n6sdtu2d1mdb4vsk2f934gk` (`company30_id`),
  KEY `FK5dn7g7uyxncavgrmu273ne5bs` (`company31_id`),
  KEY `FKpr2bmgvjr5a23ee5q7ya4my76` (`company32_id`),
  KEY `FKipqm9degbj7phd3a0pvx2l5ly` (`company33_id`),
  KEY `FK7e32n2dtrh2tu6a6414edrcou` (`company34_id`),
  KEY `FKbkrvb0eujglj86ydmxsa2mngm` (`company35_id`),
  KEY `FKr6xk16ckhq0bixim5hh1h0f2p` (`company36_id`),
  KEY `FKk12u4wacama1q79hxmdsqfeuv` (`company37_id`),
  KEY `FK73ewmey4mw74ky8kadvydivex` (`company38_id`),
  KEY `FKg3ow6b1mj3sb55gojojd6rok0` (`company39_id`),
  KEY `FKf7m81lrg0sdympo61rrcfigqh` (`company40_id`),
  KEY `FK6vvxnnfe6v2ih7v8vt8hlxl88` (`company41_id`),
  KEY `FKaqu2xm2841sw6u4wg1hb9d5g5` (`company42_id`),
  KEY `FKriq4txsp931s59ck19nm32gm2` (`company43_id`),
  KEY `FK65g2h5cayb4awcfvqyro5tl04` (`company44_id`),
  KEY `FKarvrs1e7ftlhgevftde1ng18a` (`company45_id`),
  KEY `FKtmnrw6n6dhfj0c5y0lwy312fr` (`company46_id`),
  KEY `FKe09bgli6f973gcxlbblu45txx` (`company47_id`),
  KEY `FKfe4uj6mr27ma6hgujvels3gtt` (`company48_id`),
  KEY `FKiyltcynxl9rnvy9xftmlmvxkv` (`company49_id`),
  KEY `FKmgefl9mxbk2f285ki2htch00v` (`company50_id`),
  KEY `FKcq1fmeieic94onh3g50edn7gg` (`company51_id`),
  KEY `FKsflrtwk4m4dy5iuntqdyah33k` (`company52_id`),
  KEY `FKacvkil29ga14hkbx53upl2h6g` (`company53_id`),
  KEY `FKc35dyugmw8ru51g9ffb9wh1he` (`company54_id`),
  KEY `FK61on2sr20qlpshvbqcdoq4cxn` (`company55_id`),
  KEY `FKl84u162x7e506kr518u6rps6c` (`company56_id`),
  KEY `FKa1dfouvny189t12r603uvhrvr` (`company57_id`),
  KEY `FKnerbm9ryaw25p8p534wt48ytw` (`company58_id`),
  KEY `FK4ljhqvngfybvww6mrxhyf0j35` (`company59_id`),
  KEY `FKbws2vkao9ixx2gr6grb57vlok` (`company60_id`),
  KEY `FK41rnxmtl1702ajdsokthqkv91` (`company61_id`),
  KEY `FK79l7i1dfs2fpl0mwna0yr8tou` (`company62_id`),
  CONSTRAINT `FK16ha652i3audy2ijs3r18kla6` FOREIGN KEY (`company25_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FK1pbupffx26skdvluxy4cxmpiy` FOREIGN KEY (`oneCompany_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FK22n6sdtu2d1mdb4vsk2f934gk` FOREIGN KEY (`company30_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FK2ve9vwaog26al4ptifnvwpnfl` FOREIGN KEY (`company29_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FK3fppbvryta115yvs0nk8ckei8` FOREIGN KEY (`company21_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FK41rnxmtl1702ajdsokthqkv91` FOREIGN KEY (`company61_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FK4ljhqvngfybvww6mrxhyf0j35` FOREIGN KEY (`company59_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FK5dn7g7uyxncavgrmu273ne5bs` FOREIGN KEY (`company31_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FK61on2sr20qlpshvbqcdoq4cxn` FOREIGN KEY (`company55_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FK65g2h5cayb4awcfvqyro5tl04` FOREIGN KEY (`company44_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FK6vvxnnfe6v2ih7v8vt8hlxl88` FOREIGN KEY (`company41_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FK73ewmey4mw74ky8kadvydivex` FOREIGN KEY (`company38_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FK79l7i1dfs2fpl0mwna0yr8tou` FOREIGN KEY (`company62_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FK7e32n2dtrh2tu6a6414edrcou` FOREIGN KEY (`company34_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FK7ghid732wnwhf6tftc9sqnlyx` FOREIGN KEY (`twelveCompany_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FK7hf101w4xe7bxrp4vtkt1a50n` FOREIGN KEY (`company24_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FK9jlg9dtiryhm27a0ihli7pfmx` FOREIGN KEY (`sevenCompany_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FK9tsysdo6nxkf1dd1fupskx7kq` FOREIGN KEY (`nineCompany_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FK9yw7tv6k1jvjmmkvrvttuu9o2` FOREIGN KEY (`company23_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FKa1dfouvny189t12r603uvhrvr` FOREIGN KEY (`company57_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FKacvkil29ga14hkbx53upl2h6g` FOREIGN KEY (`company53_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FKaqu2xm2841sw6u4wg1hb9d5g5` FOREIGN KEY (`company42_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FKarvrs1e7ftlhgevftde1ng18a` FOREIGN KEY (`company45_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FKbkrvb0eujglj86ydmxsa2mngm` FOREIGN KEY (`company35_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FKbws2vkao9ixx2gr6grb57vlok` FOREIGN KEY (`company60_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FKc35dyugmw8ru51g9ffb9wh1he` FOREIGN KEY (`company54_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FKcq1fmeieic94onh3g50edn7gg` FOREIGN KEY (`company51_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FKd06d0dbcictgjtivhpynmpg44` FOREIGN KEY (`fifteenCompany_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FKe09bgli6f973gcxlbblu45txx` FOREIGN KEY (`company47_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FKe27nytws4aq4p12kngjupjpk1` FOREIGN KEY (`sexteenCompany_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FKek0qjtqpcpu1hlu8eal5vyicr` FOREIGN KEY (`eightCompany_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FKekto6b0d3g62ls7lu8gjq5iuo` FOREIGN KEY (`fourteenCompany_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FKf7m81lrg0sdympo61rrcfigqh` FOREIGN KEY (`company40_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FKfdeij7de7thhxk36ikx90dcn2` FOREIGN KEY (`fourCompany_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FKfe4uj6mr27ma6hgujvels3gtt` FOREIGN KEY (`company48_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FKg3ow6b1mj3sb55gojojd6rok0` FOREIGN KEY (`company39_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FKh81y1fqjq50vutjwdsa896w75` FOREIGN KEY (`sexCompany_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FKhu1ia8uk4cjypfssv49nsco1l` FOREIGN KEY (`company22_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FKipqm9degbj7phd3a0pvx2l5ly` FOREIGN KEY (`company33_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FKirbusr3y51hd5xqh3hfoh3wip` FOREIGN KEY (`tenCompany_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FKiyk9nn4qtv5ev29s9x4i6vyl2` FOREIGN KEY (`thirteenCompany_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FKiyltcynxl9rnvy9xftmlmvxkv` FOREIGN KEY (`company49_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FKjqr1k05s0ifrdybl78yik6pup` FOREIGN KEY (`ninteenCompany_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FKjwm573y4vbhmx49pmjq0xfxnd` FOREIGN KEY (`seventeenCompany_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FKk12u4wacama1q79hxmdsqfeuv` FOREIGN KEY (`company37_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FKl84u162x7e506kr518u6rps6c` FOREIGN KEY (`company56_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FKlsl6srh5ytr26qrl3utydw3nj` FOREIGN KEY (`company26_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FKmgefl9mxbk2f285ki2htch00v` FOREIGN KEY (`company50_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FKmwy35hw8rg1b48iqmumum5pka` FOREIGN KEY (`twoCompany_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FKnerbm9ryaw25p8p534wt48ytw` FOREIGN KEY (`company58_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FKo3gre64yx2rd8vu2id7ufaeix` FOREIGN KEY (`eighteenCompany_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FKovlm0ew67yjq00vv2t1i3iiy5` FOREIGN KEY (`company27_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FKpof3nfpaphir4t1k9sx418auy` FOREIGN KEY (`company20_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FKpr2bmgvjr5a23ee5q7ya4my76` FOREIGN KEY (`company32_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FKq0jth5fml3vgjqgguxj30hy5s` FOREIGN KEY (`tweentyCompany_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FKq1c7uopujhpyanuu1gfntyasw` FOREIGN KEY (`elevenCompany_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FKqoj55o5evcr2ysi4c5yk4qeq` FOREIGN KEY (`company28_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FKr6xk16ckhq0bixim5hh1h0f2p` FOREIGN KEY (`company36_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FKriq4txsp931s59ck19nm32gm2` FOREIGN KEY (`company43_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FKs65u1e99qgl11wv1jk34k677m` FOREIGN KEY (`threeCompany_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FKsflrtwk4m4dy5iuntqdyah33k` FOREIGN KEY (`company52_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FKsg0lkgsydsnhrajt0t2e381kw` FOREIGN KEY (`fifheCompany_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FKtmnrw6n6dhfj0c5y0lwy312fr` FOREIGN KEY (`company46_id`) REFERENCES `company` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mainPageCompanys`
--

LOCK TABLES `mainPageCompanys` WRITE;
/*!40000 ALTER TABLE `mainPageCompanys` DISABLE KEYS */;
INSERT INTO `mainPageCompanys` VALUES (371,403,NULL,420,388,448,383,437,408,NULL,367,398,NULL,393,454,414,431,378,NULL,426,373,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `mainPageCompanys` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `offers`
--

DROP TABLE IF EXISTS `offers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `offers` (
  `id` bigint(20) NOT NULL,
  `isApproved` bit(1) NOT NULL,
  `brandName` varchar(255) DEFAULT NULL,
  `contactDetails` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `endDate` date DEFAULT NULL,
  `headLine` varchar(255) DEFAULT NULL,
  `offerImage` varchar(255) DEFAULT NULL,
  `startDate` date DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK1t5y4jml1cqslvq6hooi0810p` (`company_id`),
  CONSTRAINT `FK1t5y4jml1cqslvq6hooi0810p` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `offers`
--

LOCK TABLES `offers` WRITE;
/*!40000 ALTER TABLE `offers` DISABLE KEYS */;
INSERT INTO `offers` VALUES (370,'','The Boulevard Arjaan','https://www.rotanamagicalseason.com/amman/theboulevardarjaanbyrotana/159','','2018-01-07','Christmas Market at The Boulevard','60443b06-34aa-4da9-9abf-3228582bec49','2017-12-07',367),(376,'',' Media Plus','https://www.facebook.com/events/170440337032653/','','2017-12-22','Christmas Parade in Jordan','9fbdec9c-6dc4-4c04-957c-9e15609b8fb1','2017-12-17',373),(381,'','Hackmanite','http://www.jordansun.com/events/calendar/12-nights-to-christmas-hackmanite/','','2017-12-26','12 Nights to Christmas','7feda713-eedf-41bd-bad6-144a2e64cbb5','2017-12-14',378),(396,'','Sameh Mall','http://samehgroup.com/Weakly','','2018-01-16','End year offers','d5b84c7e-0f94-46b4-b8c0-4aa45516308f','2018-01-10',393),(401,'','C - Town','http://ctown.jo/offers','','2018-02-28','Back to School offer','36f5fb99-3fc4-427b-b580-5fc372c887f5','2018-01-25',398),(406,'','Al Farid Stores','http://www.alfaridstores.com/promo.php','','2018-02-06','Week offer','7a326b61-ad54-4ada-9017-18600174a4fe','2018-01-31',403),(412,'','City Mall','http://www.citymall.jo/','','2017-12-30','Spend the greatest time at our Christmas Climb','4fa306d8-38ef-4a87-a4af-3bde5524d250','2017-12-07',408),(418,'','TAJ Lifestyle Mall','http://www.tajlifestyle.com/node/1710','','2017-12-31','Operation North Pole!','f389de10-8816-4594-99c9-baa328591453','2017-12-04',414),(424,'','Al Abdali Mall','http://www.abdalimall.com/event-details.aspx?id=99','','2017-12-31','The Nutcraker Christmas','e4ed4712-eeb7-449c-bff0-ca2beac7c908','2017-12-07',420),(429,'\0','Royal Jordanian','https://www.rj.com/en/special-offers/dec-offer/jordan','','2017-12-20','Merry Reasons to Travel','74bcbc2f-94ac-443f-880d-119645acb215','2017-12-03',426),(435,'\0','RATTAN HOUSE','www.rattan-house.com','','2018-01-31','We have value collections On sale and clearance','7e101ebb-41f3-40d5-9f11-bb3e062fc4a4','2017-12-01',431),(441,'\0','Beit Shams','https://www.facebook.com/events/529344694086387/','','2018-01-18','Christmas Open House at Beit Shams','d4c5a1e2-a04e-41ab-86de-8823fe1dc768','2017-11-28',437),(446,'\0','A Little Italy Christmas','https://www.facebook.com/events/131557930850253/','Every Friday & Saturday ','2017-12-23','A Little Italy Christmas','9d9b51d0-c4dc-4497-a38c-674367dc3b10','2017-12-01',443),(452,'\0','IKEA','http://www.ikea.com/jo/en/','','2018-01-08','Guess what? We have more iamazing tems on sale','671f3249-d081-4492-9746-a9a47ee5ac6f','2017-12-14',448),(458,'\0','Essentials Spa','https://essentialsamman.com/promotions-specials/','','2017-12-31','Ladies, our monthly December holiday offer is here!','1c45e58e-a356-4fb6-a321-df00fb308742','2017-12-01',454),(462,'\0','C - Town','http://ctown.jo/offers','','2018-01-31','Week offer','9c93cb1d-727d-4c0a-b31b-42c9cfe9e699','2018-01-25',398),(474,'\0','Carrefour Haypermarket','http://flipbooks.azurewebsites.net/Flipbooks/jor7feb.pdf','','2018-02-20','Offers','e5edbad0-b318-466a-9f6c-574e7cbbdd5b','2018-02-07',383),(476,'\0','Carrefour Supermarket','http://flipbooks.azurewebsites.net/Flipbooks/jormk7fb.pdf','','2018-02-20','Offers','c7205d9d-5956-4724-b481-0710df5841a9','2018-02-07',388),(480,'\0','IKEA','http://www.ikea.com/jo/en/store/amman/special_offers?cid=ba|jo|products_we_love_fy18|201801110716560173_1','','2018-02-19','Products we love for irresistible prices ','f4a3fb59-2760-46d1-be65-6835f032b953','2018-01-15',448),(482,'\0','IKEA','http://www.ikea.com/ms/en_JO/ikea-family/better-deals/index.html','','2018-01-31','IKEA Family Offers','d0c493b7-2ee7-4252-9b59-b4b5a4f7cfc6','2018-01-14',448),(544,'\0','Al Farid Stores','http://www.alfaridstores.com/promo.php','','2018-02-06','Week offer','3c391e3e-c1dd-4eb2-80b5-da29a74abc13','2018-01-31',403);
/*!40000 ALTER TABLE `offers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `offers_locations`
--

DROP TABLE IF EXISTS `offers_locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `offers_locations` (
  `Offer_id` bigint(20) NOT NULL,
  `locations_id` bigint(20) NOT NULL,
  PRIMARY KEY (`Offer_id`,`locations_id`),
  KEY `FKqiugvjtev3i3cyo6jh4lt0sbr` (`locations_id`),
  CONSTRAINT `FKkmmjj2occ8balsi00ng76y3bj` FOREIGN KEY (`Offer_id`) REFERENCES `offers` (`id`),
  CONSTRAINT `FKqiugvjtev3i3cyo6jh4lt0sbr` FOREIGN KEY (`locations_id`) REFERENCES `locations` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `offers_locations`
--

LOCK TABLES `offers_locations` WRITE;
/*!40000 ALTER TABLE `offers_locations` DISABLE KEYS */;
INSERT INTO `offers_locations` VALUES (370,365),(376,365),(381,365),(396,365),(401,365),(406,365),(429,365),(462,365),(474,365),(476,365),(544,365),(412,409),(418,415),(446,415),(424,421),(435,432),(441,438),(452,449),(480,449),(482,449),(458,455);
/*!40000 ALTER TABLE `offers_locations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sample`
--

DROP TABLE IF EXISTS `sample`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sample` (
  `id` bigint(20) NOT NULL,
  `imageUrl` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sample`
--

LOCK TABLES `sample` WRITE;
/*!40000 ALTER TABLE `sample` DISABLE KEYS */;
/*!40000 ALTER TABLE `sample` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sample_locations`
--

DROP TABLE IF EXISTS `sample_locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sample_locations` (
  `Sample_id` bigint(20) NOT NULL,
  `locations_id` bigint(20) NOT NULL,
  PRIMARY KEY (`Sample_id`,`locations_id`),
  UNIQUE KEY `UK_j1s1onk1cublquvoqfw55q1ao` (`locations_id`),
  CONSTRAINT `FKcubns14vckh7xjq34nw91hjxm` FOREIGN KEY (`Sample_id`) REFERENCES `sample` (`id`),
  CONSTRAINT `FKj1yf4erve8b4xwp74rh278713` FOREIGN KEY (`locations_id`) REFERENCES `locations` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sample_locations`
--

LOCK TABLES `sample_locations` WRITE;
/*!40000 ALTER TABLE `sample_locations` DISABLE KEYS */;
/*!40000 ALTER TABLE `sample_locations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `staticInfo`
--

DROP TABLE IF EXISTS `staticInfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staticInfo` (
  `id` bigint(20) NOT NULL,
  `aboutUs` longtext,
  `marketingMessage` longtext,
  `policy` longtext,
  `termAndCondition` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staticInfo`
--

LOCK TABLES `staticInfo` WRITE;
/*!40000 ALTER TABLE `staticInfo` DISABLE KEYS */;
INSERT INTO `staticInfo` VALUES (9,'\n<br>\nDeals On Hub is an online marketing place where companies can post their sales, offers & promotions, and guide the shoppers find product deals that make significant savings for them.\n<br>\n<br>\nShoppers can search on this site for the sales, offers and promotions from various categories such as fashion, electronics, homeware, furniture, tourism, financial services, Automobiles and other products, where it updates continuously.\n<br>\n<br>\nWith Deals On Hub, no need now to check the market personally any more for sales, offers & promotions.\n<br>\n<br>\nsupport@dealsonhub.com\n\n\n','Be the first to know about the best deals in Amman',NULL,NULL);
/*!40000 ALTER TABLE `staticInfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subCategories`
--

DROP TABLE IF EXISTS `subCategories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subCategories` (
  `id` bigint(20) NOT NULL,
  `isApproved` bit(1) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `subCategoryName` varchar(255) DEFAULT NULL,
  `category_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKn6kfb91nqe8wsl6k7o1i8vxpj` (`category_id`),
  CONSTRAINT `FKn6kfb91nqe8wsl6k7o1i8vxpj` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subCategories`
--

LOCK TABLES `subCategories` WRITE;
/*!40000 ALTER TABLE `subCategories` DISABLE KEYS */;
INSERT INTO `subCategories` VALUES (25,'','b1aa4f67-a269-491d-a84f-04b9161c24aa','Fashion',11),(26,'','50cfe8de-71e4-47aa-8964-476aa74448e2','Health & Beauty',11),(27,'','80216f70-6de5-49eb-972f-f2e75dbc4a4c','Electronics',11),(28,'','f0d6eaef-a0e7-429e-a0f3-a255c9a90bd3','Watches',11),(29,'','7ac128ad-4322-4e4c-b44e-4aa5edc671e3','Optics',11),(30,'','0072fabc-44a0-4038-b2a2-b3cdac4db766','Furniture',11),(31,'\0',NULL,'Homeware',11),(32,'','f216c678-e84c-44d7-bd34-952c11f86105','Hotels',13),(33,'','974fe2e5-bf81-4bd1-86db-0282ef874807','Flights',13),(34,'','d0189063-0b27-48c4-a4d8-18ba49ed3ee0','Travel agencies',13),(35,'','886b9fad-3530-43d1-92d0-cbb4c3f1a718','Banks',15),(36,'','afe5e8e4-2bd1-4506-a708-de4988f06f30','Money Exchange',15),(37,'','d7ff2f5f-799d-4203-bf32-f45d8b508a79','Car agencies ',17),(38,'','8f469994-2e0c-4712-82a1-39978c42da5a','Car rental',17),(197,'','3857c76a-b6e2-4657-86c5-380bad259d10','Cinemas ',19),(198,'','340dc90d-133a-43c4-929a-40565edbf644','Health Clubs',19),(199,'','78e731ba-488a-4003-a217-ea9f973dd28b','Dance Classes ',19),(200,'','7990eb67-85cd-47ee-858e-fa7357635528','Adventure Parks',19),(201,'','da98d7dc-2f86-4c36-b965-34df5ac0378f','Bowling',19),(202,'','4f36c059-76ae-4977-877c-0d0eb654d9f1','Kids Toys',19),(247,'','b3729620-0b6a-4cb5-a8ed-49c18e60345d','',245),(249,'','e5c3324a-85a2-4894-876a-dbbd6f42b4a8','Training courses',245),(318,'','cc7a83b3-96c1-4ded-95ec-3158714bd5d7','Malls',238),(319,'','432b786a-c0b0-4641-a6e8-9c51e32abfa2','Hypermarkets',238),(328,'','8a2d6aff-6328-412f-8226-26e7bad127ff','Shoes & Bags',11),(337,'','29eb8afc-3497-4ccc-a577-356dea00048e','Sport',19),(339,'','5bf53e8c-8550-4a35-8b46-725300015d95','Events',19);
/*!40000 ALTER TABLE `subCategories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) NOT NULL,
  `birthdate` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `fullName` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `sex` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usersTable`
--

DROP TABLE IF EXISTS `usersTable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usersTable` (
  `id` bigint(20) NOT NULL,
  `firstName` varchar(255) NOT NULL,
  `lastName` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `securityRole` varchar(255) NOT NULL,
  `userName` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usersTable`
--

LOCK TABLES `usersTable` WRITE;
/*!40000 ALTER TABLE `usersTable` DISABLE KEYS */;
INSERT INTO `usersTable` VALUES (2,'Admin','User','21232f297a57a5a743894a0e4a801fc3','ADMIN','admin'),(3,'Super Admin','User','af151a8e61b2e9f45b2a9a5d04f9e67d','SUPER_ADMIN','super');
/*!40000 ALTER TABLE `usersTable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vacancies`
--

DROP TABLE IF EXISTS `vacancies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vacancies` (
  `id` bigint(20) NOT NULL,
  `isApproved` bit(1) NOT NULL,
  `companyName` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL,
  `requirements` varchar(255) DEFAULT NULL,
  `responsibilities` varchar(255) DEFAULT NULL,
  `imageUrl` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vacancies`
--

LOCK TABLES `vacancies` WRITE;
/*!40000 ALTER TABLE `vacancies` DISABLE KEYS */;
/*!40000 ALTER TABLE `vacancies` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-10 17:18:47
